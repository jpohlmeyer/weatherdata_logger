#!/usr/bin/env python3

import argparse
import serial
import time


class SerialLogger:

    # COMMAND = "data"
    # TIMEOUT_IN_SEC = 2

    def __init__(self, arduino_port, baudrate):
        self.running = True
        self.arduino = None
        self.arduino_port = arduino_port
        self.baudrate = baudrate

    def connect_to_arduino(self):
        self.arduino = serial.Serial(port=self.arduino_port, baudrate=self.baudrate, timeout=.1)
        time.sleep(2)

    # def get_data(self):
    #     self.arduino.write(bytes(self.COMMAND, 'utf-8'))
    #     time.sleep(1)
    #     data = self.read_data_from_arduino()
    #     if data:
    #         return self.parse_values(data)
    #     else:
    #         return None

    def read_data_from_arduino(self):
        data = self.arduino.readline()
        if data:
            try:
                decoded_bytes = data[0:len(data) - 2].decode("utf-8")
                return decoded_bytes
            except:
                return None

    # def wait_for_timeout(self):
    #     until = datetime.now() + timedelta(seconds=self.TIMEOUT_IN_SEC)
    #     while self.running and datetime.now() < until:
    #         time.sleep(1)

    def run(self):
        self.connect_to_arduino()
        if not self.arduino or not self.arduino.is_open:
            print("device not connected")
            self.running = False
        while self.running:
            if self.arduino and self.arduino.is_open:
                data = self.read_data_from_arduino()
                if data:
                    print("{timestamp},{data}".format(timestamp=time.time(), data=data))
            else:
                print("(Restarting arduino connection")
                self.connect_to_arduino()
        self.stop()

    def stop(self):
        if self.arduino and self.arduino.is_open:
            self.arduino.close()


def main():
    parser = argparse.ArgumentParser(description="")
    parser.add_argument('--arduino-port', type=str, help='arduino port address',
                        default='/dev/ttyACM0')
    parser.add_argument('--baudrate', type=str, help='arduino serial connection baudrate',
                        default='9600')
    args = parser.parse_args()
    arduino_port = args.arduino_port
    baudrate = args.baudrate
    serialLogger = SerialLogger(arduino_port=arduino_port, baudrate=baudrate)
    serialLogger.run()


if __name__ == "__main__":
    main()
