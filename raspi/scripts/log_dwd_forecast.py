import logging
import argparse
import sqlite3
import time

from rest_app.weather_forecast.dwd import DWDApiV16

TABLE_NAME = "dwd_forecast"


def main() -> None:
    logging.basicConfig(format='%(levelname)s: %(message)s', level=logging.INFO)
    parser = argparse.ArgumentParser(description="")
    parser.add_argument('--station_id', type=str, help='dwd weatherstation id',
                        default='H174')
    parser.add_argument('--db', type=str, help='path to sqlite db file',
                        default='dwd_forecast.sqlite')
    args = parser.parse_args()
    station_id = args.station_id
    db = args.db
    dwdapi = DWDApiV16(station_id)

    data = dwdapi.get_station_data()

    if data:
        db_con = sqlite3.connect(db)
        db_cursor = db_con.cursor()
        db_command = f"CREATE TABLE IF NOT EXISTS {TABLE_NAME} ( \
                                        id INTEGER PRIMARY KEY AUTOINCREMENT, \
                                        log_timestamp INTEGER, \
                                        forecast_made_timestamp INTEGER, \
                                        forecast_for_date_timestamp INTEGER, \
                                        temperature REAL, \
                                        humidity REAL, \
                                        dewpoint REAL, \
                                        precipitation_total REAL, \
                                        precipitation_probability REAL, \
                                        wind_speed REAL, \
                                        wind_gusts REAL, \
                                        wind_direction REAL);"
        db_cursor.execute(db_command)
        current_timestamp = int(time.time())
        for i in range(0, 24):
            forecast_for_date_timestamp = data.forecast_made + 3600 * (i + 1)
            db_cursor.execute(f"INSERT INTO {TABLE_NAME} (log_timestamp, \
                                            forecast_made_timestamp, \
                                            forecast_for_date_timestamp, \
                                            temperature, \
                                            humidity, \
                                            dewpoint, \
                                            precipitation_total, \
                                            precipitation_probability, \
                                            wind_speed, \
                                            wind_gusts, \
                                            wind_direction) \
                                        VALUES ({current_timestamp},{data.forecast_made}, \
                                            {forecast_for_date_timestamp},{data.forecast_24h_temperature[i]},{data.forecast_24h_humidity[i]}, \
                                            {data.forecast_24h_dewpoint[i]},{data.forecast_24h_precipitation_total[i]},{data.forecast_24h_precipitation_probability[i]}, \
                                            {data.forecast_24h_wind_speed[i]},{data.forecast_24h_wind_gusts[i]},{data.forecast_24h_wind_direction[i]});")
        db_con.commit()
        db_con.close()


if __name__ == "__main__":
    main()
