import logging
import argparse
import sqlite3
import time

from rest_app.weather_forecast.dwd import DWDApiV16

TABLE_NAME = "dwd_measurements"


def main() -> None:
    logging.basicConfig(format='%(levelname)s: %(message)s', level=logging.INFO)
    parser = argparse.ArgumentParser(description="")
    parser.add_argument('--station_id', type=str, help='dwd weatherstation id',
                        default='H174')
    parser.add_argument('--db', type=str, help='path to sqlite db file',
                        default='dwd_measurements.sqlite')
    args = parser.parse_args()
    station_id = args.station_id
    db = args.db
    dwdapi = DWDApiV16(station_id)

    data = dwdapi.get_station_data()

    if data:
        db_con = sqlite3.connect(db)
        db_cursor = db_con.cursor()
        db_command = f"CREATE TABLE IF NOT EXISTS {TABLE_NAME} ( \
                                        id INTEGER PRIMARY KEY AUTOINCREMENT, \
                                        temperature REAL, \
                                        humidity REAL, \
                                        dewpoint REAL, \
                                        timestamp_measurement INTEGER, \
                                        timestamp_log INTEGER);"
        db_cursor.execute(db_command)
        current_timestamp = int(time.time())
        db_cursor.execute(f"INSERT INTO {TABLE_NAME} (temperature, humidity, dewpoint, timestamp_measurement, timestamp_log) \
                                    VALUES ({data.current_temperature}, {data.current_humidity}, {data.current_dewpoint}, {data.current_timestamp}, {current_timestamp});")
        db_con.commit()
        db_con.close()


if __name__ == "__main__":
    main()
