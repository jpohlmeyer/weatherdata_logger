#!/bin/bash

ma_venv/bin/gunicorn -b '[::]:8080' -w 2 "rest_app.wsgi:application"
