# module to run flask app using gunicorn

from rest_app import app
from rest_app.app import app as application

with application.app_context():
    app.db.create_all()
