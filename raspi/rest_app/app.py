import datetime
import math
import time
from dateutil import tz
import logging
from flask import Flask, request, jsonify, make_response, redirect
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.exc import OperationalError
from sqlalchemy.sql import func
import os
import numpy

from rest_app.weather_forecast.dwd import DWDApiV16

# define database filename
DATABASE_FILENAME = 'database.sqlite'

SECONDS_TIL_NOTIFY = 60 * 10
TEMP_DIFF_NOTIFY = 10

# init flask and sqlalchemy
app = Flask(__name__)
basedir = os.path.abspath(os.getcwd())
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///' + os.path.join(basedir, DATABASE_FILENAME)
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
db = SQLAlchemy(app)

# set logging level to debug
app.logger.setLevel(logging.DEBUG)


# DATABASE MODELS

class SensorData(db.Model):
    """Entity model for the SensorData provided by the DataLogger."""
    id: db.Column = db.Column(db.Integer, primary_key=True)
    t_outdoor_sht: db.Column = db.Column(db.Float)
    h_outdoor_sht: db.Column = db.Column(db.Float)
    t_indoor_scd: db.Column = db.Column(db.Float)
    h_indoor_scd: db.Column = db.Column(db.Float)
    co2: db.Column = db.Column(db.Float)
    occupied: db.Column = db.Column(db.Boolean)
    timestamp: db.Column = db.Column(db.Integer)

    def __repr__(self):
        me = self.as_serializable()
        return f'SensorData(id={me["id"]},' \
               f't_outdoor_sht={me["t_outdoor_sht"]},h_outdoor_sht={me["h_outdoor_sht"]},' \
               f't_indoor_scd={me["t_indoor_scd"]},h_indoor_scd={me["h_indoor_scd"]},' \
               f'co2={me["co2"]},occupied={me["occupied"]},timestamp={me["timestamp"]}'

    def as_serializable(self) -> dict:
        return {
            "id": self.id,
            "t_outdoor_sht": self.t_outdoor_sht,
            "h_outdoor_sht": self.h_outdoor_sht,
            "t_indoor_scd": self.t_indoor_scd,
            "h_indoor_scd": self.h_indoor_scd,
            "co2": self.co2,
            "occupied": self.occupied,
            "timestamp": self.timestamp
        }


class WindowData(db.Model):
    """Entity model for the WindowData provided by the DataLogger."""
    id: db.Column = db.Column(db.Integer, primary_key=True)
    windowstate: db.Column = db.Column(db.String)
    timestamp: db.Column = db.Column(db.Integer)

    def __repr__(self):
        me = self.as_serializable()
        return f'WindowData(id={me["id"]},windowstate={me["windowstate"]},' \
               f'timestamp={me["timestamp"]})'

    def as_serializable(self) -> dict:
        return {
            "id": self.id,
            "windowstate": self.windowstate,
            "timestamp": self.timestamp
        }


class ComputedProperties(db.Model):
    """Entity model for computed properties."""
    id: db.Column = db.Column(db.Integer, primary_key=True)
    timestamp: db.Column = db.Column(db.Integer)
    rolling_avg_outdoor_temp: db.Column = db.Column(db.Float)

    def __repr__(self):
        me = self.as_serializable()
        return f'ComputedProperties(id={me["id"]},' \
               f'timestamp={me["timestamp"]},' \
               f'rolling_avg_outdoor_temp={me["rolling_avg_outdoor_temp"]}'

    def as_serializable(self) -> dict:
        return {
            "id": self.id,
            "timestamp": self.timestamp,
            "rolling_avg_outdoor_temp": self.rolling_avg_outdoor_temp
        }


class AppPreferences(db.Model):
    id: db.Column = db.Column(db.Integer, primary_key=True)
    safety: db.Column = db.Column(db.Float)
    energycomfort: db.Column = db.Column(db.Float)
    health: db.Column = db.Column(db.Float)
    weatherstation: db.Column = db.Column(db.String)

    def __repr__(self):
        me = self.as_serializable()
        return f'Preferences(id={me["id"]},' \
               f'safety={me["safety"]},' \
               f'energycomfort={me["energycomfort"]},' \
               f'health={me["health"]},' \
               f'weatherstation={me["weatherstation"]}'

    def as_serializable(self) -> dict:
        return {
            "id": self.id,
            "safety": self.safety,
            "energycomfort": self.energycomfort,
            "health": self.health,
            "weatherstation": self.weatherstation
        }


# APP ROUTES

# SENSORDATA ROUTES

@app.route("/sensordata", methods=["POST"])
def add_sensor_data():
    """Input sensordata into application via JSON POST Request."""
    t_outdoor_sht = request.json['t_outdoor_sht']
    h_outdoor_sht = request.json['h_outdoor_sht']
    t_indoor_scd = request.json['t_indoor_scd']
    h_indoor_scd = request.json['h_indoor_scd']
    co2 = request.json['co2']
    occupied = request.json['occupied']
    timestamp = request.json['timestamp']

    if t_outdoor_sht == "null" or h_outdoor_sht == "null" or t_indoor_scd == "null" or \
            h_indoor_scd == "null" or co2 == "null" or occupied == "null" or timestamp == "null":
        return make_response(jsonify("Not all data is provided."), 400)

    new_sensordata = SensorData(t_outdoor_sht=t_outdoor_sht, h_outdoor_sht=h_outdoor_sht,
                                t_indoor_scd=t_indoor_scd, h_indoor_scd=h_indoor_scd, co2=co2,
                                occupied=occupied, timestamp=timestamp)
    db.session.add(new_sensordata)
    __write_to_db_with_retry(db_session=db.session)

    return jsonify(new_sensordata.as_serializable())


@app.route("/sensordata", methods=["GET"])
def get_sensor_data():
    """Get all sensordata as JSON."""
    time_filter_from = request.args.get('from', default=None, type=int)
    time_filter_to = request.args.get('to', default=None, type=int)
    if time_filter_from is None:
        time_filter_from = int(time.time()) - 2 * 60 * 60
    if time_filter_to is None:
        time_filter_to = int(time.time())
    queried_sensordata = SensorData.query.filter(
        time_filter_from <= SensorData.timestamp, SensorData.timestamp <= time_filter_to).order_by(
        SensorData.timestamp.asc()).all()
    if not queried_sensordata:
        return make_response(jsonify("No sensordata found."), 404)
    sensordata_as_serializable = [sensordata.as_serializable() for sensordata in queried_sensordata]
    for entry in sensordata_as_serializable:
        entry["dewpoint_indoor"] = __calculate_dewpoint(entry["t_indoor_scd"], entry["h_indoor_scd"])
        entry["dewpoint_outdoor"] = __calculate_dewpoint(entry["t_outdoor_sht"], entry["h_outdoor_sht"])
    return jsonify(sensordata_as_serializable)


@app.route("/sensordata/latest", methods=["GET"])
def get_latest_sensor_data():
    """Get single latest sensordata entry as JSON."""
    latest_sensordata = SensorData.query.order_by(SensorData.timestamp.desc()).first()
    if latest_sensordata:
        sensordata_as_serializable = latest_sensordata.as_serializable()
        sensordata_as_serializable["dewpoint_indoor"] = __calculate_dewpoint(sensordata_as_serializable["t_indoor_scd"],
                                                                             sensordata_as_serializable["h_indoor_scd"])
        sensordata_as_serializable["dewpoint_outdoor"] = __calculate_dewpoint(
            sensordata_as_serializable["t_outdoor_sht"],
            sensordata_as_serializable["h_outdoor_sht"])
        return jsonify(sensordata_as_serializable)
    else:
        return make_response(jsonify("No latest sensordata found."), 404)


def __calculate_dewpoint(temp: float, hum: float) -> float:
    a = 17.625
    b = 243.04
    c = numpy.log(hum / 100) + (a * temp) / (b + temp)
    return (b * c) / (a - c)


# WINDOWDATA ROUTES

@app.route("/windowdata", methods=["POST"])
def add_window_data():
    """Input windowdata into application via JSON POST Request."""
    windowstate = request.json['windowstate']
    timestamp = request.json['timestamp']

    if windowstate == "null" or timestamp == "null":
        return make_response(jsonify("Not all data is provided."), 400)

    new_windowdata = WindowData(windowstate=windowstate, timestamp=timestamp)
    db.session.add(new_windowdata)
    __write_to_db_with_retry(db_session=db.session)

    return jsonify(new_windowdata.as_serializable())


@app.route("/windowdata", methods=["GET"])
def get_window_data():
    """Get all windowdata as JSON."""
    time_filter_from = request.args.get('from', default=None, type=int)
    time_filter_to = request.args.get('to', default=None, type=int)
    if time_filter_from is None:
        time_filter_from = int(time.time()) - 2 * 60 * 60
    if time_filter_to is None:
        time_filter_to = int(time.time())
    queried_windowdata = WindowData.query.filter(
        time_filter_from <= WindowData.timestamp, WindowData.timestamp <= time_filter_to).order_by(
        WindowData.timestamp.asc()).all()
    if not queried_windowdata:
        return make_response(jsonify("No sensordata found."), 404)
    windowdata_as_serializable = [windowdata.as_serializable() for windowdata in queried_windowdata]
    return jsonify(windowdata_as_serializable)


@app.route("/windowdata/latest", methods=["GET"])
def get_latest_window_data():
    """Get single latest windowdata entry as JSON."""
    latest_windowdata = WindowData.query.order_by(WindowData.timestamp.desc()).first()
    if latest_windowdata:
        return jsonify(latest_windowdata.as_serializable())
    else:
        return make_response(jsonify("No latest windowdata found."), 404)


@app.route("/computed_properties/rolling_avg_outdoor_temp", methods=["GET"])
def get_rolling_avg_outdoor_temp():
    latest_properties = __get_rolling_avg_temp_internal()
    if not latest_properties:
        return make_response(jsonify("No properties found."), 404)
    return jsonify(latest_properties.rolling_avg_outdoor_temp.as_serializable())


def __get_rolling_avg_temp_internal() -> ComputedProperties:
    latest_properties = ComputedProperties.query.order_by(ComputedProperties.timestamp.desc()).limit(1).one_or_none()
    now = datetime.datetime.now()
    start_of_day = datetime.datetime(year=now.year, month=now.month, day=now.day, hour=0, minute=0, second=0,
                                     microsecond=0, tzinfo=tz.gettz('Europe / Berlin'))
    if latest_properties is None or latest_properties.timestamp != start_of_day.timestamp():
        # init value
        app.logger.debug("Calculate outdoor temperature rolling average.")
        avgs = []
        for day in range(0, 7):
            time_filter_from = int((start_of_day - datetime.timedelta(days=day + 1)).timestamp())
            time_filter_to = int((start_of_day - datetime.timedelta(days=day)).timestamp())
            day_avg = __calc_avg_outdoor_temp(from_timestamp=time_filter_from, to_timestamp=time_filter_to)
            app.logger.debug(f"from: {time_filter_from} to: {time_filter_to} avg: {day_avg}")
            avgs.append(day_avg)

        avg = (avgs[0] + 0.8 * avgs[1] + 0.6 * avgs[2] + 0.5 * avgs[3] + 0.4 * avgs[4] + 0.3 * avgs[5] + 0.2 * avgs[
            6]) / 3.8

        latest_properties = ComputedProperties(rolling_avg_outdoor_temp=avg,
                                               timestamp=int(start_of_day.timestamp()))
        db.session.add(latest_properties)
        __write_to_db_with_retry(db_session=db.session)

    return latest_properties


def __calc_avg_outdoor_temp(from_timestamp, to_timestamp) -> SensorData:
    return SensorData.query.with_entities(func.avg(SensorData.t_outdoor_sht).label('average')) \
        .filter(from_timestamp <= SensorData.timestamp) \
        .filter(to_timestamp > SensorData.timestamp).first()[0]


STATION_ID = "H174"


@app.route("/weather_forecast", methods=["GET"])
def get_weather_forecast():
    station_id = request.args.get('station_id', default=STATION_ID, type=str)
    dwdapi = DWDApiV16(station_id)
    data = dwdapi.get_station_data()
    if data:
        return jsonify(data.as_serializable())
    return make_response(jsonify("Getting weather forecast failed."), 500)


@app.route("/airing_forecast", methods=["GET"])
def calculate_airing_forecast():
    latest_windowdata_query = WindowData.query.order_by(WindowData.timestamp.desc()).first()
    latest_sensordata_query = SensorData.query.order_by(SensorData.timestamp.desc()).first()
    if not latest_windowdata_query or not latest_sensordata_query:
        return make_response(jsonify("Computing Airing Forecast failed."), 500)

    latest_windowdata = latest_windowdata_query.as_serializable()
    latest_sensordata = latest_sensordata_query.as_serializable()
    change_window_data_query = WindowData.query.filter(
        WindowData.windowstate != latest_windowdata["windowstate"]).order_by(
        WindowData.timestamp.desc()).first()
    if not change_window_data_query:
        change_window_data_query = WindowData(id=0, windowstate="CLOSED", timestamp=0)
    change_window_data = change_window_data_query.as_serializable()

    thirty_min_prior_secs = int(time.time()) - 30 * 60
    if latest_windowdata["windowstate"] != "CLOSED":
        prior_filter_end = change_window_data["timestamp"]
        prior_filter_start = change_window_data["timestamp"] - 30 * 60
    elif change_window_data["timestamp"] > thirty_min_prior_secs:
        prior_filter_end = int(time.time())
        prior_filter_start = change_window_data["timestamp"]
    else:
        prior_filter_end = int(time.time())
        prior_filter_start = thirty_min_prior_secs

    prior_sensordata_query = SensorData.query.filter(
        prior_filter_end >= SensorData.timestamp, SensorData.timestamp >= prior_filter_start).order_by(
        SensorData.timestamp.asc()).all()
    if not prior_sensordata_query:
        return make_response(jsonify("Computing Airing Forecast failed."), 500)

    prior_sensordata = [sensordata.as_serializable() for sensordata in prior_sensordata_query]
    notify = abs(latest_sensordata["t_indoor_scd"] - latest_sensordata["t_outdoor_sht"]) > TEMP_DIFF_NOTIFY and \
             latest_windowdata["windowstate"] != "CLOSED" \
             and time.time() - change_window_data["timestamp"] > SECONDS_TIL_NOTIFY

    avg_co2_rise = 0
    for i in range(1, len(prior_sensordata)):
        avg_co2_rise += prior_sensordata[i]["co2"] - prior_sensordata[i - 1]["co2"]
    hourly_co2_rise = avg_co2_rise * (3600 / (prior_filter_end - prior_filter_start))
    preferences_query = AppPreferences.query.order_by(AppPreferences.id.desc()).first()

    if not preferences_query:
        preferences_query = AppPreferences(safety=1 / 3, energycomfort=1 / 3, health=1 / 3,
                                     weatherstation=STATION_ID)
    preferences = preferences_query.as_serializable()

    dwdapi = DWDApiV16(preferences["weatherstation"])
    station_data = dwdapi.get_station_data()

    airing_forecast = list()
    (previous_data, _) = __compute_airing_forecast_dict(
        timestamp=prior_sensordata[0]["timestamp"], wind_speed=station_data.current_forecasted_wind_gusts,
        rain_probability=station_data.current_forecasted_precipitation_probability,
        t_indoor=prior_sensordata[0]["t_indoor_scd"], h_indoor=prior_sensordata[0]["h_indoor_scd"],
        t_outdoor=prior_sensordata[0]["t_outdoor_sht"], h_outdoor=prior_sensordata[0]["h_outdoor_sht"],
        co2=prior_sensordata[0]["co2"], safety_factor=preferences["safety"],
        energy_factor=preferences["energycomfort"],
        health_factor=preferences["health"])
    airing_forecast.append(previous_data)
    (data, co2_maxed_out) = __compute_airing_forecast_dict(
        timestamp=latest_sensordata["timestamp"], wind_speed=station_data.current_forecasted_wind_gusts,
        rain_probability=station_data.current_forecasted_precipitation_probability,
        t_indoor=latest_sensordata["t_indoor_scd"], h_indoor=latest_sensordata["h_indoor_scd"],
        t_outdoor=latest_sensordata["t_outdoor_sht"], h_outdoor=latest_sensordata["h_outdoor_sht"],
        co2=latest_sensordata["co2"], safety_factor=preferences["safety"],
        energy_factor=preferences["energycomfort"],
        health_factor=preferences["health"])
    airing_forecast.append(data)
    first_forecasted_co2 = latest_sensordata["co2"] + \
                           ((station_data.forecast_made + 3600 - latest_sensordata[
                               "timestamp"]) / 3600.0) * hourly_co2_rise
    start_index = 1 if latest_sensordata["timestamp"] >= station_data.forecast_made + 3600 else 0
    predicted_co2 = 770 if co2_maxed_out else first_forecasted_co2  # used when resetting co2 after maximum
    # predicted_co2 = first_forecasted_co2
    for hour in range(start_index, len(station_data.forecast_24h_temperature)):
        (data, co2_maxed_out_local) = __compute_airing_forecast_dict(
            timestamp=station_data.forecast_made + 3600 * (hour + 1),
            wind_speed=station_data.forecast_24h_wind_gusts[hour],
            rain_probability=station_data.forecast_24h_precipitation_probability[hour],
            t_indoor=latest_sensordata["t_indoor_scd"], h_indoor=latest_sensordata["h_indoor_scd"],
            t_outdoor=station_data.forecast_24h_temperature[hour],
            h_outdoor=station_data.forecast_24h_humidity[hour],
            co2=predicted_co2, safety_factor=preferences["safety"],
            energy_factor=preferences["energycomfort"],
            health_factor=preferences["health"])
        airing_forecast.append(data)
        if co2_maxed_out_local:  # used when resetting co2 after maximum
            predicted_co2 = 770
        else:
            predicted_co2 += hourly_co2_rise

    return dict({"airingForecast": airing_forecast, "windowInfo": dict(
        {"windowstate": latest_windowdata["windowstate"], "lastChange": change_window_data["timestamp"],
         "notify": notify, "occupied": latest_sensordata["occupied"], "beforeAiringCo2": prior_sensordata[-1]["co2"],
         "currentCo2": latest_sensordata["co2"]})})


IDEAL_TEMP = 20
IDEAL_HUM = 45
IDEAL_DEW = __calculate_dewpoint(IDEAL_TEMP, IDEAL_HUM)  # 7.7


def __compute_airing_forecast_dict(timestamp, wind_speed, rain_probability, t_indoor, h_indoor, t_outdoor, h_outdoor,
                                   co2, safety_factor, energy_factor, health_factor):
    safety_value = __calculate_safety_factor(timestamp=timestamp, wind_speed=wind_speed,
                                             rain_probability=rain_probability)
    energy_comfort_value = __calculate_energy_comfort_factor(t_outdoor=t_outdoor, h_outdoor=h_outdoor)
    (health_value, co2_maxed_out) = __calculate_health_factor(co2=co2, t_indoor=t_indoor, h_indoor=h_indoor,
                                                              t_outdoor=t_outdoor, h_outdoor=h_outdoor)
    total_value = safety_factor * safety_value + energy_factor * energy_comfort_value + health_factor * health_value
    return {"timestamp": timestamp,
            "total": total_value,
            "safety": safety_value,
            "energy_comfort": energy_comfort_value,
            "health": health_value}, co2_maxed_out


def __calculate_safety_factor(timestamp, wind_speed, rain_probability):
    s1 = -1
    if datetime.datetime.fromtimestamp(timestamp, tz=tz.gettz('Europe / Berlin')).hour > 6:
        # s1 = 0
        s1 = 1

    # s2 = 0
    s2 = 1
    if wind_speed > 50:
        s2 = -1
    elif wind_speed > 30:
        -0.1 * (wind_speed - 40)
        # -0.05 * (wind_speed - 30)

    s3 = 1
    # s3 = 0
    if rain_probability > 70:
        s3 = -1
    elif rain_probability > 30:
        s3 = -0.05 * (rain_probability - 50)
        # s3 = -0.025 * (rain_probability - 30)

    return min(min(s1, s2), s3)


def __calculate_energy_comfort_factor(t_outdoor, h_outdoor):
    outdoor_dew = __calculate_dewpoint(t_outdoor, h_outdoor)
    # indoor_dew = __calculate_dewpoint(t_indoor, h_indoor)
    # sd_dew2 = 2
    # ce1_2 = 1 - 2 * math.exp(-math.pow(indoor_dew - IDEAL_DEW, 2) / (2 * math.pow(sd_dew2, 2)))
    # ce_1 = 0.5 * ce1_1 + 0.5 * ce1_2
    # sd_temp2 = 4
    # ce2_2 = 1 - 2 * math.exp(-math.pow(t_indoor - IDEAL_TEMP, 2) / (2 * math.pow(sd_temp2, 2)))
    # ce_2 = 0.5 * ce2_1 + 0.5 * ce2_2
    sd_dew1 = 3.5
    ce1_1 = 2 * math.exp(-math.pow(outdoor_dew - IDEAL_DEW, 2) / (2 * math.pow(sd_dew1, 2))) - 1

    sd_temp1 = 7
    ce2_1 = 2 * math.exp(-math.pow(t_outdoor - IDEAL_TEMP, 2) / (2 * math.pow(sd_temp1, 2))) - 1
    return 0.5 * ce1_1 + 0.5 * ce2_1


def __calculate_health_factor(co2, t_indoor, h_indoor, t_outdoor, h_outdoor):
    h1 = 0
    if co2 > 1770:
        h1 = 1
    elif co2 > 970:
        h1 = (co2 - 970) / 800

    outdoor_dew = __calculate_dewpoint(t_outdoor, h_outdoor)
    indoor_dew = __calculate_dewpoint(t_indoor, h_indoor)

    dew_shift = (IDEAL_DEW - indoor_dew) / 2

    sd1 = 3.5
    h2 = 2 * math.exp(-math.pow(outdoor_dew - (IDEAL_DEW + dew_shift), 2) / (2 * math.pow(sd1, 2))) - 1
    return 0.7 * h1 + 0.3 * h2, h1 >= 1


def __write_to_db_with_retry(db_session):
    for attempt in range(5):
        try:
            db_session.commit()
        except OperationalError:
            app.logger.warning("Database connection failed. Retry adding data.")
        else:
            break
    else:
        app.logger.error("Database connection failed after 5 retries.")


@app.route("/app_preferences", methods=["POST"])
def set_app_preferences():
    safety = request.json['safety']
    energyComfort = request.json['energycomfort']
    health = request.json['health']
    weatherstation = request.json['weatherstation']

    new_preferences = AppPreferences(safety=safety, energycomfort=energyComfort, health=health,
                                     weatherstation=weatherstation)
    db.session.add(new_preferences)
    __write_to_db_with_retry(db_session=db.session)
    logging.warning("saved preferences: " + str(new_preferences.as_serializable()))
    return jsonify(new_preferences.as_serializable())


# MAIN


def main():
    """Run app on the Flask development server."""
    db.create_all()
    app.run(host='0.0.0.0', port=8080, debug=True)
