#!/usr/bin/env python3

import rest_app.app

if __name__ == "__main__":
    rest_app.app.main()