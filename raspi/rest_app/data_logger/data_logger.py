#!/usr/bin/env python3

import logging
import json
from datetime import datetime, timedelta
from typing import Optional
import serial
import time
import requests


class DataLogger:
    """Read sensor data from Serial connection from arduino or brix and log to a REST API"""

    # command to request data from arduino
    COMMAND = "data"

    # identify command and answers
    IDENTIFY_COMMAND = "identify"
    IDENTIFY_ARDUINO = "ARDUINO"
    IDENTIFY_BRIX = "BRIX"

    def __init__(self, device_port: str = "/dev/ttyACM0",
                 baudrate: int = 9600, rest_host="localhost", rest_port=8080,
                 timeout_between_readings: int = 15):
        self.device = None
        self.device_type = None
        self.device_port = device_port
        self.baudrate = baudrate
        self.rest_host = rest_host
        self.rest_port = rest_port
        self.timeout_between_readings = timeout_between_readings

    def run(self) -> None:
        """Run datalogger until interrupted."""
        # connect device
        if not self._try_connect_to_device():
            logging.error("Connection to Device failed on port %s and baudrate %s.", self.device_port, self.baudrate)
            self.close_device_connection()
            return
        self.device_type = self._identify_device()
        if self.device_type:
            logging.info("Identified device %s", self.device_type)
            # log data until running is False
            self._log_data()
        else:
            logging.error("Unknown device %s.", self.device_type)
            self.close_device_connection()

    def _try_connect_to_device(self) -> bool:
        self.device = serial.Serial(port=self.device_port, baudrate=self.baudrate, timeout=.1)
        time.sleep(2)
        return self.device and self.device.is_open

    def _identify_device(self) -> Optional[str]:
        self.device.write(bytes(self.IDENTIFY_COMMAND, 'utf-8'))
        time.sleep(1)
        raw_data = self.device.readline()
        if raw_data:
            data = self._utf8_decode_data(raw_data)
            if data:
                return data
        return None

    def _log_data(self) -> None:
        # Run until interrupted
        while True:
            if self.device and self.device.is_open:
                data = self._read_data_from_device()
                if data:
                    self._post_data_to_rest(data)
                else:
                    logging.error("Reading data failed.")
                self._wait_for_timeout()
            else:
                logging.error("Device %s connection lost, restarting connection.", self.device_type)
                self._try_connect_to_device()

    def _post_data_to_rest(self, data) -> None:
        # log data to REST API
        if self.device_type == self.IDENTIFY_ARDUINO:
            ressource = "sensordata"
        else:
            ressource = "windowdata"
        url = f"http://{self.rest_host}:{self.rest_port}/{ressource}"

        for attempt in range(5):
            try:
                response = requests.post(url=url,json=data)
                if response.status_code == 200:
                    logging.debug("logged data: %s", response.json())
                elif response.status_code == 400:
                    logging.warning("tried to log incomplete data: %s", response.json())
                else:
                    raise requests.exceptions.RequestException()
            except requests.exceptions.RequestException:
                logging.warning("Logging data to REST API failed. Retry logging data.")
            else:
                break
        else:
            logging.error("Logging data to REST API failed after 5 retries.")


    def _read_data_from_device(self) -> Optional[dict]:
        # write command to Serial and wait for an answer
        # then read until newline, decode utf8 and parse JSON to dict
        self.device.write(bytes(self.COMMAND, 'utf-8'))
        time.sleep(1)
        raw_data = self.device.readline()
        if raw_data:
            data = self._utf8_decode_data(raw_data)
            if data:
                return self._parse_values(data)
        return None

    def _utf8_decode_data(self, raw_data: bytes) -> Optional[str]:
        # decode bytes as utf8
        try:
            decoded_bytes = raw_data[0:len(raw_data) - 2].decode("utf-8")
            return decoded_bytes
        except UnicodeDecodeError:
            return None

    def _parse_values(self, data) -> dict:
        # parse json as dict and add a timestamp
        data_dict = json.loads(data)
        data_dict["timestamp"] = int(time.time())
        return data_dict

    def _wait_for_timeout(self) -> None:
        # sleep until timeout is reached
        until = datetime.now() + timedelta(seconds=self.timeout_between_readings)
        while datetime.now() < until:
            time.sleep(1)

    def close_device_connection(self):
        """Close the connection to device after interrupting run loop."""
        if self.device and self.device.is_open:
            self.device.close()
