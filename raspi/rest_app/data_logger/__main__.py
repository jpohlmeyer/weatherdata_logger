import logging
import argparse
from rest_app.data_logger.data_logger import DataLogger


def main() -> None:
    """Parse arguments and start datalogging until interrupted by KeybaordInterrupt."""
    logging.basicConfig(format='%(levelname)s: %(message)s', level=logging.INFO)
    parser = argparse.ArgumentParser(description="Log data from arduino and POST to REST API.")
    parser.add_argument('--device_port', type=str, help='address of port the device is connected to',
                        default='/dev/ttyACM0')
    parser.add_argument('--device_baudrate', type=int, help='baudrate of arduino connection', default=9600)
    parser.add_argument('--rest_host', type=str, help='host of the rest server', default='localhost')
    parser.add_argument('--rest_port', type=int, help='port of the rest server', default=8080)
    parser.add_argument('--timeout', type=int, help='timeout between readings in sec', default=15)
    args = parser.parse_args()
    data_logger = DataLogger(device_port=args.device_port, baudrate=args.device_baudrate,
                             rest_host=args.rest_host, rest_port=args.rest_port, timeout_between_readings=args.timeout)

    try:
        data_logger.run()
    except KeyboardInterrupt:
        data_logger.close_device_connection()


if __name__ == "__main__":
    main()
