import requests
import datetime
from dateutil import tz


class StationDataV16:

    def __init__(self, station_id, current_temperature, current_humidity, current_dewpoint,
                 current_precipitation, current_timestamp, forecast_made,
                 current_forecasted_precipitation_probability, current_forecasted_wind_speed,
                 current_forecasted_wind_gusts, current_forecasted_wind_direction,
                 forecast_24h_temperature, forecast_24h_humidity, forecast_24h_dewpoint,
                 forecast_24h_precipitation_total, forecast_24h_precipitation_probability, forecast_24h_wind_speed,
                 forecast_24h_wind_gusts, forecast_24h_wind_direction):
        self.station_id = station_id
        self.current_temperature = current_temperature
        self.current_humidity = current_humidity
        self.current_dewpoint = current_dewpoint
        self.current_precipitation = current_precipitation
        self.current_timestamp = current_timestamp
        self.forecast_made = forecast_made
        self.current_forecasted_precipitation_probability = current_forecasted_precipitation_probability
        self.current_forecasted_wind_speed = current_forecasted_wind_speed
        self.current_forecasted_wind_gusts = current_forecasted_wind_gusts
        self.current_forecasted_wind_direction = current_forecasted_wind_direction
        self.forecast_24h_temperature = forecast_24h_temperature
        self.forecast_24h_humidity = forecast_24h_humidity
        self.forecast_24h_dewpoint = forecast_24h_dewpoint
        self.forecast_24h_precipitation_total = forecast_24h_precipitation_total
        self.forecast_24h_precipitation_probability = forecast_24h_precipitation_probability
        self.forecast_24h_wind_speed = forecast_24h_wind_speed
        self.forecast_24h_wind_gusts = forecast_24h_wind_gusts
        self.forecast_24h_wind_direction = forecast_24h_wind_direction

    def __repr__(self) -> str:
        return 'StationDataV16(station_id={station_id},' \
               'current_temperature = {current_temperature},' \
               'current_humidity = {current_humidity},' \
               'current_dewpoint = {current_dewpoint},' \
               'current_precipitation = {current_precipitation},' \
               'current_timestamp = {current_timestamp},' \
               'forecast_made={forecast_made},' \
               'current_forecasted_precipitation_probability={current_forecasted_precipitation_probability},' \
               'current_forecasted_wind_speed={current_forecasted_wind_speed},' \
               'current_forecasted_wind_gusts={current_forecasted_wind_gusts},' \
               'current_forecasted_wind_direction={current_forecasted_wind_direction},' \
               'forecast_24h_temperature = {forecast_24h_temperature},' \
               'forecast_24h_humidity = {forecast_24h_humidity},' \
               'forecast_24h_dewpoint = {forecast_24h_dewpoint},' \
               'forecast_24h_precipitation_total = {forecast_24h_precipitation_total},' \
               'forecast_24h_precipitation_probability = {forecast_24h_precipitation_probability},' \
               'forecast_24h_wind_speed = {forecast_24h_wind_speed},' \
               'forecast_24h_wind_gusts = {forecast_24h_wind_gusts},' \
               'forecast_24h_wind_direction = {forecast_24h_wind_direction}' \
               ')'.format(
            station_id=self.station_id,
            current_temperature=self.current_temperature,
            current_humidity=self.current_humidity,
            current_dewpoint=self.current_dewpoint,
            current_precipitation=self.current_precipitation,
            current_timestamp=self.current_timestamp,
            forecast_made=self.forecast_made,
            current_forecasted_precipitation_probability=self.current_forecasted_precipitation_probability,
            current_forecasted_wind_speed=self.current_forecasted_wind_speed,
            current_forecasted_wind_gusts=self.current_forecasted_wind_gusts,
            current_forecasted_wind_direction=self.current_forecasted_wind_direction,
            forecast_24h_temperature=self.forecast_24h_temperature,
            forecast_24h_humidity=self.forecast_24h_humidity,
            forecast_24h_dewpoint=self.forecast_24h_dewpoint,
            forecast_24h_precipitation_total=self.forecast_24h_precipitation_total,
            forecast_24h_precipitation_probability=self.forecast_24h_precipitation_probability,
            forecast_24h_wind_speed=self.forecast_24h_wind_speed,
            forecast_24h_wind_gusts=self.forecast_24h_wind_gusts,
            forecast_24h_wind_direction=self.forecast_24h_wind_direction
        )

    def as_serializable(self) -> dict:
        return {
            "station_id": self.station_id,
            "current_temperature": self.current_temperature,
            "current_humidity": self.current_humidity,
            "current_dewpoint": self.current_dewpoint,
            "current_precipitation": self.current_precipitation,
            "current_timestamp": self.current_timestamp,
            "forecast_made": self.forecast_made,
            "current_forecasted_precipitation_probability": self.current_forecasted_precipitation_probability,
            "current_forecasted_wind_speed": self.current_forecasted_wind_speed,
            "current_forecasted_wind_gusts": self.current_forecasted_wind_gusts,
            "current_forecasted_wind_direction": self.current_forecasted_wind_direction,
            "forecast_24h_temperature": self.forecast_24h_temperature,
            "forecast_24h_humidity": self.forecast_24h_humidity,
            "forecast_24h_dewpoint": self.forecast_24h_dewpoint,
            "forecast_24h_precipitation_total": self.forecast_24h_precipitation_total,
            "forecast_24h_precipitation_probability": self.forecast_24h_precipitation_probability,
            "forecast_24h_wind_speed": self.forecast_24h_wind_speed,
            "forecast_24h_wind_gusts": self.forecast_24h_wind_gusts,
            "forecast_24h_wind_direction": self.forecast_24h_wind_direction
        }


class DWDApiV16:
    HOST = "https://s3.eu-central-1.amazonaws.com/app-prod-static.warnwetter.de/v16"
    CURRENT_MEASUREMENT_RESOURCE = "/current_measurement_{station_id}.json"
    FORECAST_RESOURCE = "/forecast_mosmix_v2_{station_id}.json"

    def __init__(self, station_id):
        self.station_id = station_id
        current_measurement_resource = self.CURRENT_MEASUREMENT_RESOURCE.format(station_id=station_id)
        forecast_resource = self.FORECAST_RESOURCE.format(station_id=station_id)
        self.current_measurement_url = f"{self.HOST}{current_measurement_resource}"
        self.forecast_url = f"{self.HOST}{forecast_resource}"

    def get_station_data(self):
        current_response = requests.get(url=self.current_measurement_url)
        current_json = current_response.json()
        if not current_json:
            return None
        forecast_response = requests.get(url=self.forecast_url)
        forecast_json = forecast_response.json()
        if not forecast_json:
            return None

        station_id = forecast_json["forecast1"]["stationId"]
        current_temperature = current_json["temperature"] / 10
        current_humidity = current_json["humidity"] / 10
        current_dewpoint = current_json["dewpoint"] / 10
        current_precipitation = current_json["precipitation"]
        current_timestamp = int(current_json["time"] / 1000)

        forecast_made = int(forecast_json["forecastStart"] / 1000)
        current_hour = datetime.datetime.fromtimestamp(forecast_made, tz.gettz('Europe / Berlin')).hour
        current_forecasted_precipitation_probability = forecast_json["forecast1"]["precipitationProbablity"][current_hour] / 10
        current_forecasted_wind_speed = forecast_json["forecast1"]["windSpeed"][current_hour] / 10
        current_forecasted_wind_gusts = forecast_json["forecast1"]["windGust"][current_hour] / 10
        current_forecasted_wind_direction = forecast_json["forecast1"]["windDirection"][current_hour] / 10
        forecast_24h_temperature = [forecast_json["forecast1"]["temperature"][i] / 10 for i in
                                    range(current_hour + 1, current_hour + 24 + 1)]
        forecast_24h_humidity = [forecast_json["forecast1"]["humidity"][i] / 10 for i in
                                 range(0, 24)]
        forecast_24h_dewpoint = [forecast_json["forecast1"]["dewPoint2m"][i] / 10 for i in
                                 range(0, 24)]
        forecast_24h_precipitation_total = [forecast_json["forecast1"]["precipitationTotal"][i] / 10 for i in
                                            range(0, 24)]
        forecast_24h_precipitation_probability = [int(forecast_json["forecast1"]["precipitationProbablity"][i] / 10) for i in
                                                  range(current_hour + 1, current_hour + 24 + 1)]
        forecast_24h_wind_speed = [forecast_json["forecast1"]["windSpeed"][i] / 10 for i in
                                   range(current_hour + 1, current_hour + 24 + 1)]
        forecast_24h_wind_gusts = [forecast_json["forecast1"]["windGust"][i] / 10 for i in
                                   range(current_hour + 1, current_hour + 24 + 1)]
        forecast_24h_wind_direction = [int(forecast_json["forecast1"]["windDirection"][i] / 10) for i in
                                       range(current_hour + 1, current_hour + 24 + 1)]

        station_data = StationDataV16(station_id=station_id,
                                      current_temperature=current_temperature,
                                      current_humidity=current_humidity,
                                      current_dewpoint=current_dewpoint,
                                      current_timestamp=current_timestamp,
                                      current_precipitation=current_precipitation,
                                      forecast_made=forecast_made,
                                      current_forecasted_precipitation_probability=current_forecasted_precipitation_probability,
                                      current_forecasted_wind_speed=current_forecasted_wind_speed,
                                      current_forecasted_wind_gusts=current_forecasted_wind_gusts,
                                      current_forecasted_wind_direction=current_forecasted_wind_direction,
                                      forecast_24h_temperature=forecast_24h_temperature,
                                      forecast_24h_humidity=forecast_24h_humidity,
                                      forecast_24h_dewpoint=forecast_24h_dewpoint,
                                      forecast_24h_precipitation_total=forecast_24h_precipitation_total,
                                      forecast_24h_precipitation_probability=forecast_24h_precipitation_probability,
                                      forecast_24h_wind_speed=forecast_24h_wind_speed,
                                      forecast_24h_wind_gusts=forecast_24h_wind_gusts,
                                      forecast_24h_wind_direction=forecast_24h_wind_direction)
        return station_data


class StationDataV30:

    def __init__(self, station_id, start, time_step, temperature, humidity, dewpoint):
        self.station_id = station_id
        self.start = start
        self.time_step = time_step
        self.temperature = temperature
        self.humidity = humidity
        self.dewpoint = dewpoint

    def __repr__(self) -> str:
        return 'StationDataV30(station_id={station_id}, start={start}, ' \
               'time_step={time_step}, temperature={temperature}, humidity={humidity}, ' \
               'dewpoint={dewpoint})'.format(
            station_id=self.station_id, start=self.start, time_step=self.time_step, temperature=self.temperature,
            humidity=self.humidity, dewpoint=self.dewpoint
        )

    def pretty_print(self) -> str:
        return """Station Id: {station_id}
Start: {start}
Time:\tTemp\tHum\tDew:
{time_temp_hum_dew}""".format(station_id=self.station_id,
                              start=datetime.datetime.fromtimestamp(self.start / 1000,
                                                                    tz.gettz('Europe / Berlin')).strftime("%d.%m.%Y"),
                              time_temp_hum_dew=self.__pretty_print_time_temp_hum_dew())

    def __pretty_print_time_temp_hum_dew(self) -> str:
        res = []
        limit = min(len(self.temperature), len(self.humidity))
        limit = min(limit, len(self.dewpoint))
        for i in range(0, limit):
            dt = datetime.datetime.fromtimestamp(self.start / 1000 + (self.time_step * i / 1000),
                                                 tz.gettz('Europe / Berlin'))
            res.append('{time}:\t{temp}°C\t{hum}%\t{dew}°C'.format(
                time=dt.strftime("%H:%M"),
                temp=self.temperature[i] / 10,
                hum=self.humidity[i] / 10,
                dew=self.dewpoint[i] / 10
            ))
        return '\n'.join(res)


class DWDApiV30:
    HOST = "https://app-prod-ws.warnwetter.de/v30"
    RESOURCE = "/stationOverviewExtended"
    QUERY_PARAMETER = "stationIds="

    def __init__(self, station_id):
        self.station_id = station_id
        self.url = f"{self.HOST}/{self.RESOURCE}?{self.QUERY_PARAMETER}{self.station_id}"

    def get_station_data(self):
        response = requests.get(url=self.url)
        json = response.json()
        if not json:
            return None

        station_id = json[self.station_id]["forecast1"]["stationId"]
        start = json[self.station_id]["forecast1"]["start"]
        time_step = json[self.station_id]["forecast1"]["timeStep"]
        temperature = json[self.station_id]["forecast1"]["temperature"]
        humidity = json[self.station_id]["forecast1"]["humidity"]
        dewpoint = json[self.station_id]["forecast1"]["dewPoint2m"]

        station_data = StationDataV30(station_id=station_id, start=start, time_step=time_step, temperature=temperature,
                                      humidity=humidity, dewpoint=dewpoint)
        return station_data
