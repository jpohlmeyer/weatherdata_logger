import logging
import argparse
from rest_app.weather_forecast.dwd import DWDApiV16


def main() -> None:
    logging.basicConfig(format='%(levelname)s: %(message)s', level=logging.INFO)
    parser = argparse.ArgumentParser(description="")
    parser.add_argument('--station_id', type=str, help='dwd weatherstation id',
                        default='H174')
    args = parser.parse_args()
    dwdapi = DWDApiV16(args.station_id)

    station_data = dwdapi.get_station_data()
    print(station_data)


if __name__ == "__main__":
    main()
