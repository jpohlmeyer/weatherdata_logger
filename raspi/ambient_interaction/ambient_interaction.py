#!/usr/bin/env python3
import logging
import os
import signal
import time

import requests

from ambient_interaction.audio_control import AudioController
# pip rpi_ws281x has to be installed as root
# script has to be run as root
# on rpi3 LEDs can be controlled using PCM on GPIO 21 with no analogue audio interference
from ambient_interaction.led_control import LEDController, \
    PulsingGradientSingleLEDStrategy, TwoStaticColorLEDStrategy
from ambient_interaction.mute_button_control import MuteButtonController


# sound: https://freesound.org/people/felineterror/sounds/614292/

class StateMonitor:
    UPDATE_STRATEGY_TIMEOUT_SEC = 15

    def __init__(self, rest_host="localhost", rest_port=8080):
        self.rest_host = rest_host
        self.rest_port = rest_port
        self.stopped = False
        self.strategy = None
        self.ledcontroller = None
        self.audiocontroller = None
        self.mutebuttoncontroller = None
        self.current_case = None
        self.current_color = None
        signal.signal(signal.SIGINT, self.stop)
        signal.signal(signal.SIGTERM, self.stop)

    def stop(self, signum, frame):
        self.stopped = True

    def select_strategy(self, currentIAQ):
        # logging.warning("currentIAQ: " + str(currentIAQ))
        if not currentIAQ:
            self.current_case = "NONE"
            self.strategy = None
            self.ledcontroller.set_led_strategy(self.strategy)
            return
        if not currentIAQ["windowInfo"]["occupied"]:
            self.current_case = False
            self.strategy = None
            self.ledcontroller.set_led_strategy(self.strategy)
            return
        else:
            if currentIAQ["windowInfo"]["windowstate"] == "CLOSED":
                new_case = "CLOSED"
                if currentIAQ["airingForecast"][1]["total"] > 0:
                    color = (
                        int((175 * (1 - currentIAQ["airingForecast"][1]["total"]))),
                        175,
                        0)
                else:
                    color = (
                        175,
                        int((175 * (currentIAQ["airingForecast"][1]["total"] + 1))),
                        0)
                self.current_color = color
                if (currentIAQ["airingForecast"][1]["total"] > currentIAQ["airingForecast"][0]["total"] and
                        currentIAQ["airingForecast"][1]["total"] > currentIAQ["airingForecast"][2]["total"]):
                    gradient = 0
                else:
                    gradient = -1
                if self.current_case == new_case:
                    self.strategy.update_parameters(color, gradient)
                else:
                    self.strategy = PulsingGradientSingleLEDStrategy(color, gradient, 25)
                    self.ledcontroller.set_led_strategy(self.strategy)
            else:  # OPEN or TILTED
                # umlaufend von aus zu gruen wie lange man lueften soll
                if currentIAQ["windowInfo"]["currentCo2"] <= 770:
                    new_case = "AIRING_BLINKING"
                    green = (0, 175, 0)
                    gradient = 0
                    if self.current_case == new_case:
                        self.strategy.update_parameters(green, gradient)
                    else:
                        self.strategy = PulsingGradientSingleLEDStrategy(green, gradient, 25)
                        self.ledcontroller.set_led_strategy(self.strategy)
                else:
                    new_case = "AIRING_PERCENTAGE"
                    green = (0, 175, 0)
                    background_color = (175, 0, 0)
                    initialToGo = currentIAQ["windowInfo"]["beforeAiringCo2"] - 770
                    currentToGo = currentIAQ["windowInfo"]["currentCo2"] - 770
                    percentage = 1-(currentToGo / initialToGo)
                    percentage = max(percentage, 0.1)
                    if self.current_case == new_case:
                        self.strategy.update_divider(percentage)
                    else:
                        self.strategy = TwoStaticColorLEDStrategy(green, background_color, percentage)
                        self.ledcontroller.set_led_strategy(self.strategy)
            self.current_case = new_case

    def run(self):
        self.ledcontroller = LEDController()
        self.audiocontroller = AudioController()
        self.mutebuttoncontroller = MuteButtonController()
        self.mutebuttoncontroller.start_monitoring()
        while not self.stopped:
            urlBase = "http://{}:{}".format(self.rest_host, self.rest_port)
            try:
                request = requests.get(urlBase + "/airing_forecast")
                if request.status_code == 200:
                    currentIAQ = request.json()
                else:
                    currentIAQ = None
            except requests.exceptions.RequestException:
                currentIAQ = None
            self.select_strategy(currentIAQ)
            self.audiocontroller.update_audio_status(currentIAQ)
            counter = 0
            while not self.stopped and counter < self.UPDATE_STRATEGY_TIMEOUT_SEC:
                if self.mutebuttoncontroller.pressed:
                    self.audiocontroller.mute_button_pressed()
                    self.mutebuttoncontroller.start_monitoring()
                time.sleep(1)
                counter += 1
        self.mutebuttoncontroller.stop_monitoring()
        self.ledcontroller.set_led_strategy(None)
        self.audiocontroller.update_audio_status(None)


def main():
    logging.basicConfig(level=os.environ.get("LOGLEVEL", "INFO"))
    statemonitor = StateMonitor()
    statemonitor.run()
