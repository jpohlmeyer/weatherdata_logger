import random
import threading
import time

from rpi_ws281x import PixelStrip, Color


class LEDController:
    """Initializes and controls the LEDs by setting an led strategy of type AbstractLEDStrategy."""
    # LED strip configuration:
    LED_COUNT = 15  # Number of LED pixels.
    LED_PIN = 21  # GPIO pin connected to the pixels (21 uses PCM)
    LED_FREQ_HZ = 800000  # LED signal frequency in hertz (usually 800khz)
    LED_DMA = 10  # DMA channel to use for generating signal (try 10)
    LED_BRIGHTNESS = 100  # Set to 0 for darkest and 255 for brightest
    LED_INVERT = False  # True to invert the signal (when using NPN transistor level shift)
    LED_CHANNEL = 0  # set to '1' for GPIOs 13, 19, 41, 45 or 53

    def __init__(self):
        """Init the LED strip."""
        # Create NeoPixel object with appropriate configuration.
        self.strip = PixelStrip(self.LED_COUNT, self.LED_PIN, self.LED_FREQ_HZ, self.LED_DMA, self.LED_INVERT,
                                self.LED_BRIGHTNESS, self.LED_CHANNEL)
        # Intialize the library (must be called once before other functions).
        self.strip.begin()
        self.led_strategy = None

    def set_led_strategy(self, led_strategy):
        """Stop the old and set a new LED strategy for visual feedback."""
        if self.led_strategy is not None:
            self.led_strategy.stop()
        self.led_strategy = led_strategy
        if self.led_strategy is not None:
            self.led_strategy.start(self.strip)

    def __del__(self):
        if self.led_strategy is not None:
            self.led_strategy.stop()


class AbstractLEDStrategy:
    """Abstract Strategy providing base implementations for strategies."""

    def __init__(self):
        """Each concrete strategy should call super().__init__() to initialize the strategy."""
        self.strip = None
        self.stopped = False
        self.__control_thread = threading.Thread(target=self.__control_leds)

    def __control_leds(self):
        """Calls the overridden strategy method and turn the LEDs off when done."""
        self.strategy()
        self.__turn_black()

    def __turn_black(self):
        """Turn all pixels black."""
        for i in range(self.strip.numPixels()):
            self.strip.setPixelColor(i, Color(0, 0, 0))
        self.strip.show()

    def strategy(self):
        """Override this method to implement concrete behaviour. Stop strategy loop when self.stopped is True."""
        raise NotImplementedError()

    def start(self, strip):
        """Start the strategy thread."""
        if self.__control_thread is not None:
            self.strip = strip
            self.__control_thread.start()

    def stop(self):
        """Stop the strategy thread."""
        self.stopped = True
        if self.__control_thread is not None:
            self.__control_thread.join()
            self.__control_thread = None


class RandomLEDStrategy(AbstractLEDStrategy):
    """Strategy choosing a random color every 2 seconds and display it on all LEDs."""

    def strategy(self):
        iteration = 0
        while not self.stopped:
            if iteration % 20 == 0:
                self.show_color(Color(random.randint(0, 255), random.randint(0, 255), random.randint(0, 255)))
                iteration = 0
            iteration += 1
            time.sleep(0.1)

    def show_color(self, color):
        for i in range(self.strip.numPixels()):
            self.strip.setPixelColor(i, color)
        self.strip.show()


class CircleLEDStrategy(AbstractLEDStrategy):
    """Strategy update all LEDs after one another"""

    def __init__(self, start_color, end_color, time_interval):
        self.start_color = Color(start_color[0], start_color[1], start_color[2])
        self.end_color = Color(end_color[0], end_color[1], end_color[2])
        self.time_interval = time_interval
        super().__init__()

    def strategy(self):
        for pixel in range(self.strip.numPixels()):
            self.strip.setPixelColor(pixel, self.start_color)
            self.strip.show()
        while not self.stopped:
            for pixel in range(self.strip.numPixels()):
                self.strip.setPixelColor(pixel, self.end_color)
                self.strip.show()
                if self.stopped:
                    break
                else:
                    time.sleep(self.time_interval / self.strip.numPixels())


class StaticColorLEDStrategy(AbstractLEDStrategy):
    """Strategy is initialized with a specific color to show. The color will not change."""

    def __init__(self, color):
        self.color = Color(color[0], color[1], color[2])
        super().__init__()

    def strategy(self):
        while not self.stopped:
            self.show_color()
            for i in range(0, 30):
                if self.stopped:
                    break
                time.sleep(1)

    def show_color(self):
        for i in range(self.strip.numPixels()):
            self.strip.setPixelColor(i, self.color)
        self.strip.show()


class TwoStaticColorLEDStrategy(AbstractLEDStrategy):
    """Strategy is initialized with a two specific colors to show. The colors will not change."""

    def __init__(self, color1, color2, divider):
        self.color1 = Color(color1[0], color1[1], color1[2])
        self.color2 = Color(color2[0], color2[1], color2[2])
        self.divider = None
        self.new_divider = divider
        super().__init__()

    def strategy(self):
        while not self.stopped:
            if self.divider != self.new_divider:
                self.divider = self.new_divider
                self.show_color()
            time.sleep(0.5)

    def update_divider(self, divider):
        self.new_divider = divider

    def show_color(self):
        for i in range(self.strip.numPixels()):
            if self.divider > (i / self.strip.numPixels()):
                self.strip.setPixelColor(i, self.color1)
            else:
                self.strip.setPixelColor(i, self.color2)
        self.strip.show()


class PulsingSingleLEDStrategy(AbstractLEDStrategy):
    """Strategy is initialized with a specific color (r,g,b) to show and n interval in seconds how long the pulse takes."""

    UPDATE_FREQUENCY_HZ = 20

    def __init__(self, color, pulse_interval, subtract_amount):
        self.color = None
        self.color_low = None
        self.color_diff = None
        self.new_color = None
        self.new_pulse = None
        self.pulse_interval = None
        self.subtract_amount = subtract_amount
        self.update_parameters(color, pulse_interval)
        super().__init__()

    def __get_color_min(self, color_value) -> int:
        return int(color_value * self.subtract_amount / 100)

    def update_parameters(self, new_color, new_pulse):
        self.new_color = new_color
        self.new_pulse = new_pulse

    def __update_parameters_internal(self):
        self.pulse_interval = self.new_pulse
        self.color = self.new_color
        self.color_low = (
            self.__get_color_min(self.color[0]),
            self.__get_color_min(self.color[1]),
            self.__get_color_min(self.color[2])
        )
        self.color_diff = (
            self.color[0] - self.color_low[0],
            self.color[1] - self.color_low[1],
            self.color[2] - self.color_low[2]
        )

    def strategy(self):
        part = 1
        direction = -1
        while not self.stopped:
            self.__update_parameters_internal()
            if part > 1:
                part = 1
                direction = -direction
            elif part < 0:
                part = 0
                direction = -direction
            if self.pulse_interval > 0:
                color = self.__interpolate_pulse_color(part)
            else:
                color = Color(self.color[0], self.color[1], self.color[2])
            self.show_color(color)
            if self.pulse_interval > 0:
                part += direction * ((1 / self.UPDATE_FREQUENCY_HZ) / self.pulse_interval)
                time.sleep(1 / self.UPDATE_FREQUENCY_HZ)
            else:
                time.sleep(1)

    def __interpolate_pulse_color(self, part) -> Color:
        r = int(self.color_low[0] + self.color_diff[0] * part)
        g = int(self.color_low[1] + self.color_diff[1] * part)
        b = int(self.color_low[2] + self.color_diff[2] * part)
        return Color(r, g, b)

    def show_color(self, color):
        for i in range(self.strip.numPixels()):
            self.strip.setPixelColor(i, color)
        self.strip.show()


class PulsingGradientSingleLEDStrategy(AbstractLEDStrategy):
    """
    Strategy is initialized with a specific color (r,g,b) to show and a coefficient between 0 and 1 how strong the pulsing gradient is.
    0 means very smooth pulsing and 1 means very sharp color changes.
    """

    UPDATE_FREQUENCY_HZ = 20

    def __init__(self, color, gradient_coefficient, subtract_amount):
        self.color = None
        self.color_low = None
        self.color_diff = None
        self.new_color = None
        self.new_gradient = None
        self.gradient_coefficient = None
        self.pulse_interval = 3
        self.subtract_amount = subtract_amount
        self.update_parameters(color, gradient_coefficient)
        super().__init__()

    def __get_color_min(self, color_value) -> int:
        return int(color_value * self.subtract_amount / 100)

    def update_parameters(self, new_color, new_gradient):
        self.new_color = new_color
        self.new_gradient = new_gradient

    def __update_parameters_internal(self):
        self.gradient_coefficient = self.new_gradient
        self.color = self.new_color
        self.color_low = (
            self.__get_color_min(self.color[0]),
            self.__get_color_min(self.color[1]),
            self.__get_color_min(self.color[2])
        )
        self.color_diff = (
            self.color[0] - self.color_low[0],
            self.color[1] - self.color_low[1],
            self.color[2] - self.color_low[2]
        )

    def strategy(self):
        part = 1
        direction = -1
        while not self.stopped:
            self.__update_parameters_internal()
            if part > 1:
                part = 1
                direction = -direction
            elif part < 0:
                part = 0
                direction = -direction
            if self.gradient_coefficient >= 0:
                color = self.__interpolate_pulse_color(part, direction)
            else:
                color = Color(self.color[0], self.color[1], self.color[2])
            self.show_color(color)
            if self.gradient_coefficient >= 0:
                part += direction * ((1 / self.UPDATE_FREQUENCY_HZ) / self.pulse_interval)
                time.sleep(1 / self.UPDATE_FREQUENCY_HZ)
            else:
                time.sleep(1)

    def __interpolate_pulse_color(self, part, direction) -> Color:
        if direction > 0:
            # wenn direction 1:  part 0 -> 1 color change muss abgeschlossen sein, bis part = 1-gradient_coefficient?
            if self.gradient_coefficient == 1:
                color_coefficient = 1
            else:
                color_coefficient = part / (1 - self.gradient_coefficient)
        else:
            # wenn direction -1: part 1 -> 0 color change muss abgeschlossen sein, bis part = gradient_coefficient?
            if self.gradient_coefficient == 1:
                color_coefficient = 0
            else:
                color_coefficient = (part - self.gradient_coefficient) / (1 - self.gradient_coefficient)
        if color_coefficient > 1:
            color_coefficient = 1
        elif color_coefficient < 0:
            color_coefficient = 0
        r = int(self.color_low[0] + self.color_diff[0] * color_coefficient)
        g = int(self.color_low[1] + self.color_diff[1] * color_coefficient)
        b = int(self.color_low[2] + self.color_diff[2] * color_coefficient)
        return Color(r, g, b)

    def show_color(self, color):
        for i in range(self.strip.numPixels()):
            self.strip.setPixelColor(i, color)
        self.strip.show()
