import RPi.GPIO as GPIO
import time
import threading


class MuteButtonController:

    def __init__(self):
        GPIO.setmode(GPIO.BCM)
        GPIO.setup(17, GPIO.IN)
        self.stopped = False
        self.pressed = False
        self.__control_thread = None

    def __monitor_button(self):
        # idle while button is not pressed
        while GPIO.input(17) == 1 and not self.stopped:
            time.sleep(0.1)
        if not self.stopped:
            self.pressed = True

    def start_monitoring(self):
        self.stop_monitoring()
        self.stopped = False
        self.pressed = False
        self.__control_thread = threading.Thread(target=self.__monitor_button)
        self.__control_thread.start()

    def stop_monitoring(self):
        self.stopped = True
        if self.__control_thread is not None:
            self.__control_thread.join()
            self.__control_thread = None

