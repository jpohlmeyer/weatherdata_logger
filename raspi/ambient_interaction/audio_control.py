import logging
import os
import subprocess
import sys
import time


class AudioController:

    def __init__(self):
        self.media_player_process = None
        self.stopped = False

    def update_audio_status(self, current_iaq):
        if current_iaq is None or not current_iaq["windowInfo"]["occupied"]:
            if self.media_player_process:
                self.media_player_process.terminate()
                self.media_player_process = None
            return
        if current_iaq["windowInfo"]["windowstate"] == "CLOSED":
            self.stopped = False
            if self.media_player_process:
                self.media_player_process.terminate()
                self.media_player_process = None
        elif self.media_player_process is None and not self.stopped and current_iaq["windowInfo"]["notify"]:
            logging.warning("Playing sound because window is open for a long time.")
            command = [sys.executable, os.path.dirname(__file__) + "/media_player_process.py"]
            username = "pi"
            environment = os.environ
            environment["HOME"] = "/home/" + username
            environment["USER"] = username
            environment["XDG_RUNTIME_DIR"] = "/run/user/1000"
            self.media_player_process = subprocess.Popen(command, user=username, group=username,
                                                         env=environment)

    def mute_button_pressed(self):
        if self.media_player_process:
            self.media_player_process.terminate()
            self.media_player_process = None
        self.stopped = True

    def __del__(self):
        if self.media_player_process:
            self.media_player_process.terminate()
            self.media_player_process = None
