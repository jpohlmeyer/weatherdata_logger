import os
import time
import vlc


class MediaPlayerProcess:
    MP3_FILE_NAME = "614292__felineterror__the-cold.mp3"

    def __init__(self):
        media_list = vlc.MediaList()
        media_list.add_media("file://" + os.path.dirname(__file__) + "/" + self.MP3_FILE_NAME)
        self.player = vlc.MediaListPlayer()
        self.player.set_media_list(media_list)
        self.player.set_playback_mode(vlc.PlaybackMode.loop)

    def __del__(self):
        if self.player:
            self.player.stop()


if __name__ == "__main__":
    mpp = MediaPlayerProcess()
    mpp.player.play()
    while True:
        time.sleep(1)
