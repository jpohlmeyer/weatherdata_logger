#include <Wire.h>

// Library for SHT40
#include <Adafruit_SHT4x.h>

// Library for SCD30
#include <Adafruit_SCD30.h>

// Library for JSON data
#include <ArduinoJson.h>

// temp and hum sensor
Adafruit_SHT4x sht4 = Adafruit_SHT4x();

// co2 sensor
Adafruit_SCD30 scd30 = Adafruit_SCD30();

const int pir_pin = 7;

boolean occupied = false;
int occupied_counter = 0;
int occupied_threshold_minutes = 15;

void setup() {
  Serial.begin(9600);

  while (!Serial)
    delay(10);     // will pause Zero, Leonardo, etc until serial console opens

  // turn off RX LED
  pinMode(LED_BUILTIN_RX,INPUT);

  pinMode(pir_pin, INPUT);
  
  if (!sht4.begin()) {
    Serial.println("Couldn't find SHT4x");
    while (1) delay(10);
  }
  sht4.setPrecision(SHT4X_HIGH_PRECISION);
  sht4.setHeater(SHT4X_NO_HEATER);

  if (!scd30.begin()) {
    Serial.println("Failed to find SCD30 chip");
    while (1) { delay(10); }
  }

  if (!scd30.setMeasurementInterval(10)){
    Serial.println("Failed to set measurement interval");
    while(1){ delay(10);}
  }
  if (!scd30.setAltitudeOffset(118)){
    Serial.println("Failed to set altitude offset");
    while(1){ delay(10);}
  }
  if (!scd30.setTemperatureOffset(100)){ // 1 degrees celcius
    Serial.println("Failed to set temperature offset");
    while(1){ delay(10);}
  }

  // wait before starting logging
  delay(100);
}

// wait in a loop for requests for data
void loop() {
  // check if Serial connection is available
  if (Serial.available()) {
    // read command from Serial
    String command = Serial.readStringUntil('\n');

    // respond to data command
    if (command.equals("data")) {
      print_sensor_data();
    } else if (command.equals("identify")) {
      Serial.println("ARDUINO");
    } else {
      Serial.println("Invalid command");
    }
  }

  int pir_status = digitalRead(pir_pin);
  if (pir_status == LOW) {
    occupied = true;
    occupied_counter = 0;
  } else {
    if (occupied_counter >= occupied_threshold_minutes * 10 * 60) {
      occupied = false;
    } else {
      occupied_counter++;
    }
  }

  // wait before starting again
  delay(100);
}

// read sensor data and print to Serial
void print_sensor_data() {
  DynamicJsonDocument json_data(1024);
  sensors_event_t humidity, temp;
  read_sht40(&humidity, &temp);
  json_data["t_outdoor_sht"] = temp.temperature;
  json_data["h_outdoor_sht"] = humidity.relative_humidity;
  if (scd30.dataReady() && scd30.read()) {
    json_data["t_indoor_scd"] = scd30.temperature;
    json_data["h_indoor_scd"] = scd30.relative_humidity;
    json_data["co2"] = scd30.CO2;
  } else {
    json_data["t_indoor_scd"] = "null";
    json_data["h_indoor_scd"] = "null";
    json_data["co2"] = "null";
  }
  json_data["occupied"] = occupied;
  serializeJson(json_data, Serial);
  Serial.println();
}

bool read_sht40(sensors_event_t *humidity, sensors_event_t *temp) {
  return sht4.getEvent(humidity, temp);// populate temp and humidity objects with fresh data
}
