#include <Wire.h> // If you need Wire.h ALWAYS include it first!
#include <InertialSensor.h>
#include <BRIX2.h>
#include "brix_windowstate.h"

// BRIX has to be attached to the window as follows:
// - For now only windows that open from the left side are supported.
// - BRIX has to be attached on the left side of the window/frame, at least a few centimeters above the bottom corner.
// - Y axis points directly down
// - X axis points to the left
// - Z axis points into the room
// - (or in short: USB on the right, extension connectors toward the room.
// - window has to be in closed state when BRIX is started

// Generate Sensor Object and BRIX2 Object
InertialSensor inertialSensor;
BRIX2 brix2;

windowstate windowState;
int tiltCounter;
int tiltClosedCounter;
int openCounter;
int openAcc;

// full open threshold
int fot = 400;
// full open acc threshold
int foat = 12000;
// titlted threshold
int tt = -700;
// titlted threshold seconds
int tts = 2;
int tts_cycles = tts*10;

int rx_avg = -1;
int ry_avg = -1;
int rz_avg = -1;


void setup(){
  brix2.initialize();
  inertialSensor.initialize();
  windowState = CLOSED;
  tiltCounter = 0;
  tiltClosedCounter = 0;
  openCounter = 0;
  openAcc = 0;

  Serial.begin(9600);
  delay(2000);
}

void loop(){
  // +/- 2G
  int ax = inertialSensor.getAccelerationX(-20000,20000);
  int ay = inertialSensor.getAccelerationY(-20000,20000);
  int az = inertialSensor.getAccelerationZ(-20000,20000);

//  int rx_sensor_offset = 3550;

  // +/- 250 degrees per second
  int rx = inertialSensor.getRotationX(-25000,25000);// - rx_sensor_offset;
  int ry = inertialSensor.getRotationY(-25000,25000);
  int rz = inertialSensor.getRotationZ(-25000,25000);

  if (rx_avg == -1) {
    rx_avg = rx;
  } else {
    rx_avg = rx_avg * 0.99 + rx * 0.01;
  }
  rx = rx - rx_avg;

  if (ry_avg == -1) {
    ry_avg = ry;
  } else {
    ry_avg = ry_avg * 0.99 + ry * 0.01;
  }
  ry = ry - ry_avg;

  if (rz_avg == -1) {
    rz_avg = rz;
  } else {
    rz_avg = rz_avg * 0.99 + rz * 0.01;
  }
  rz = rz - rz_avg;

  // CLOSED if accumulated rx values under -fot reach -foat
  if (windowState == OPEN && rx < -fot) {
    openAcc += rx;
    if (openAcc < -foat) {
      windowState = CLOSED;
      openAcc = 0;
      openCounter = 0;
    }
  }

  // OPEN if accumulated rx values over fot reach foat
  if (windowState == CLOSED && rx > fot) {
    openAcc += rx;
    if (openAcc > foat) {
      windowState = OPEN;
      openAcc = 0;
      openCounter = 0;
    }
  }

  // reset unfinished opening or closing situation after tts seconds if not completed
  if (openAcc != 0 && openCounter > tts_cycles) {
    openAcc = 0;
    openCounter = 0;
  } else {
    openCounter++;
  }

  // TILTED if az < tt for more than tts seconds
  if (windowState != TILTED && az < tt) {
    if (tiltCounter > tts_cycles) {
      windowState = TILTED;
      tiltCounter = 0;
    } else {
      tiltCounter++;
    }
  } else {
    tiltCounter = 0;
  }

  // CLOSED FROM TILTED if az > tt for more than tts seconds
  if (windowState == TILTED && az > tt) {
    if (tiltClosedCounter > tts_cycles) {
      windowState = CLOSED;
      tiltClosedCounter = 0;
    } else {
      tiltClosedCounter++;
    }
  } else {
    tiltClosedCounter = 0;
  }
  
  if(Serial.available()){
    String command = Serial.readStringUntil('\n');
    
    if (command.equals("data")) {
      if (windowState == CLOSED) {
        Serial.println("{\"windowstate\": \"CLOSED\"}");
      } else if (windowState == TILTED) {
        Serial.println("{\"windowstate\": \"TILTED\"}");
      } else if (windowState == OPEN) {
        Serial.println("{\"windowstate\": \"OPEN\"}");
      }
    } else if (command.equals("identify")) {
      Serial.println("BRIX");
    } else {
      Serial.println("Invalid command");
    }
  }
  delay(100);
}
