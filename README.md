# Weatherdata Logger

## Basic structure

### arduino

Arduino sketches to read and provide sensor readings.

- brix_windowstate: Inform about current window status using BRIX2 inertial sensors.
- air_sensor_data: Inform about current air properties and occupied state.

### raspi

Programs to run on the Raspberry Pi.

- ambient_interaction: Control LEDs, Audio and listen for button presses.
- rest_app: 
  - app.py: central communication hub
  - data_logger: read data from serial an publish to rest
  - weather_forecast: provide current and future weatherdata from the DWD API
- scripts: scripts to start gunicorn process and for logging data for analysis or testing
- services: systemd services to start the programs

## Getting started

If not already done, upload sketches to the BRIX2 and Arduino microcontrollers.

Connect the BRIX2 and Arduino to Raspberry Pi. Make sure the BRIX2 is powered on.

If not already done, install the provided systemd services. You can enable them for autostart. 

Start services:

```shell
sudo systemctl start weatherdata-gunicorn.service
sudo systemctl start weatherdata-datalogger-0.service
sudo systemctl start weatherdata-datalogger-1.service
sudo systemctl start weatherdata-ambient-interaction.service
```